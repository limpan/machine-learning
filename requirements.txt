Requirement already satisfied: tensorflow-addons==0.18.0 in /home/linus/.pyenv/versions/3.9.18/lib/python3.9/site-packages (0.18.0)
Requirement already satisfied: packaging in /home/linus/.pyenv/versions/3.9.18/lib/python3.9/site-packages (from tensorflow-addons==0.18.0) (20.9)
Requirement already satisfied: typeguard>=2.7 in /home/linus/.pyenv/versions/3.9.18/lib/python3.9/site-packages (from tensorflow-addons==0.18.0) (2.13.3)
Requirement already satisfied: pyparsing>=2.0.2 in /home/linus/.pyenv/versions/3.9.18/lib/python3.9/site-packages (from packaging->tensorflow-addons==0.18.0) (3.1.1)
