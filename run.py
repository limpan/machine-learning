import numpy as np
import os

from tflite_model_maker.config import ExportFormat
from tflite_model_maker import model_spec
from tflite_model_maker import object_detector

import tensorflow as tf
assert tf.__version__.startswith('2')

tf.get_logger().setLevel('ERROR')
from absl import logging
logging.set_verbosity(logging.ERROR)

tflite_filename='efficientdet-lite-kvitto.tflite'
label_filename='kvitto-labels.txt'

label_map = { 1: 'tape', 2: 'tape_written' }

train_images_dir = 'dataset/train/images'
train_annotations_dir = 'dataset/train/annotations'
val_images_dir = 'dataset/validate/images'
val_annotations_dir = 'dataset/validate/annotations'

train_data = object_detector.DataLoader.from_pascal_voc(train_images_dir, train_annotations_dir, label_map=label_map)
validation_data = object_detector.DataLoader.from_pascal_voc(val_images_dir, val_annotations_dir, label_map=label_map)
test_data = object_detector.DataLoader.from_pascal_voc(val_images_dir, val_annotations_dir, label_map=label_map)

print(f'train count: {len(train_data)}')
print(f'validation count: {len(validation_data)}')
print(f'test count: {len(test_data)}')

# spec = object_detector.EfficientDetLite0Spec()

# model = object_detector.create(train_data=train_data, model_spec=spec, validation_data=validation_data, epochs=50, batch_size=3, train_whole_model=True)

# model.evaluate(test_data)

# model.export(export_dir='.', tflite_filename=tflite_filename, label_filename=label_filename, export_format=[ExportFormat.TFLITE, ExportFormat.LABEL])

# model.evaluate_tflite(tflite_filename, test_data)

import random

filenames = os.listdir(os.path.join(val_images_dir))
random_index = random.randint(0, len(filenames)-1)
INPUT_IMAGE = os.path.join(val_images_dir, filenames[random_index])

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import tflite_runtime.interpreter as tflite 
from pycoral.adapters import common
from pycoral.adapters import detect
from pycoral.utils.dataset import read_label_file

def draw_objects(draw, objs, scale_factor, labels):
  """Draws the bounding box and label for each object."""
  for obj in objs:
    bbox = obj.bbox
    color = (235, 66, 35)
    draw.rectangle([(bbox.xmin * scale_factor, bbox.ymin * scale_factor),
                    (bbox.xmax * scale_factor, bbox.ymax * scale_factor)],
                   outline=color, width=3)
    font = ImageFont.truetype("LiberationSans-Regular.ttf", size=15)
    draw.text((bbox.xmin * scale_factor + 4, bbox.ymin * scale_factor + 4),
              '%s\n%.2f' % (labels.get(obj.id, obj.id), obj.score),
              fill=color, font=font)

# Load the TF Lite model
labels = read_label_file(label_filename)
interpreter = tflite.Interpreter(tflite_filename)
interpreter.allocate_tensors()

# Resize the image for input
image = Image.open(INPUT_IMAGE)
_, scale = common.set_resized_input(
    interpreter, image.size, lambda size: image.resize(size, Image.LANCZOS))

# Run inference
interpreter.invoke()
objs = detect.get_objects(interpreter, score_threshold=0.4, image_scale=scale)

# print(objs)

# Resize again to a reasonable size for display
display_width = 500
scale_factor = display_width / image.width
height_ratio = image.height / image.width
image = image.resize((display_width, int(display_width * height_ratio)))
draw_objects(ImageDraw.Draw(image), objs, scale_factor, labels)
image.show()